
# Vagrant Demo Project

This project is for documenting and testing vagrant/ansible installation for use across multiple environments.  The easiest environment (and currently the only one supported) is Qemu/KVM on Linux (specifically Fedora 33).  Support for other environments can be developed as needed.

## Tools

- Git.  Installation on Linux should be easy with the default package managers.  Windows and OSX are also suuported, see the git website for more information: https://git-scm.com/

- A hypervisor.  Currently Qemu/KVM (Linux only) is being supported.  Vagrant can also support VirtualBox (which is technically the default for Vagrant), Hyper-v in Windows, and VMware (which costs money).  VirtualBox on Linux, OSX, and Windows could be supported in the future.

- Vagrant, a 'command line utility for managing the lifecycle of virtual machines.'  This is used as a tool for replicating server environments on local developers computers.  This tool is supported on Linux, Windows, and OSX.  Vagrant documentation can be found here: https://www.vagrantup.com/docs

- Ansible, 'an IT automation tool. It can configure systems, deploy software, and orchestrate more advanced IT tasks...'.  Ansible can configure Linux, OSX, Windows, and other targets, but it will only run from a 'Nix/OSX host.  It can run from the WSL, and it is possible to support this entire stack of tools on Windows using the WSL in the future. Ansible documentation can be found here: https://docs.ansible.com/ansible/latest/index.html:


## Workflow & Details

todo?

## Environments

### Linux / Libvirt

Fedora Workstation: https://getfedora.org/

Vagrant-Libvirt: https://github.com/vagrant-libvirt/vagrant-libvirt

Run the following to install the dependencies with Ansible:

```bash
ansible-playbook local-setup.yml [--tags=vagrantfile] [--check]
```

If you run the playbook with the tag `vagrantfile`, a Vagrantfile that configures the hostmanager plugin will be installed in your home directory.

Using the --check flag will run the playbook in check mode, showing what would be changed but not make any changes.

#### Gotchas:
- vagrant can't find default network (virbr0)?
Possibly create network manually?  Use virt-manager to check networks?

### WSL / Virtualbox

About: https://docs.microsoft.com/en-us/windows/wsl/about

todo?

### WSL / Hyper-v

todo?
